import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonMenuButton,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useParams } from 'react-router'
import ExploreContainer from '../components/ExploreContainer'
import './Page.css'
import { useLang, useTrans } from '../hooks/i18n'

function Content() {
  const { t, lang } = useTrans()
  return (
    <div>
      <h2>{t('Title')}</h2>
      <p>{t('content')}</p>
      <p>{lang}</p>
    </div>
  )
}

const Page: React.FC = () => {
  const { name } = useParams<{ name: string }>()

  const { lang, setLang } = useLang()

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen className="ion-padding">
        <h1>{name}</h1>

        {lang}

        <IonRadioGroup value={lang} onIonChange={e => setLang(e.detail.value)}>
          {['zh', 'en'].map(lang => (
            <IonItem key={lang}>
              <IonRadio slot="start" value={lang}></IonRadio>
              <IonLabel>{lang}</IonLabel>
            </IonItem>
          ))}
        </IonRadioGroup>

        <Content />
        <Content />
        <Content />
        <Content />
      </IonContent>
    </IonPage>
  )
}

export default Page
