This repo demo how to support multi-language UI (i18n)

Usage see:
[src/pages/Page.tsx](Page.tsx)

Language setup see:
[src/hooks/i18n.tsx](i18n.tsx)
