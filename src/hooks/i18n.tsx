import { useCallback } from 'react'
import { useStorageState } from 'react-use-storage-state'

export function useLang() {
  const [lang, setLang] = useStorageState<Lang>('lang', 'en')
  return { lang, setLang }
}

let zh = {
  content: '內容',
  Title: '標題',
}

let en = {
  ...zh,
}

for (let key in en) {
  let word = key as Word
  en[word] = word
}

let langs = { en, zh }

type Lang = keyof typeof langs
type Word = keyof typeof langs[Lang]

export function useTrans() {
  const { lang, setLang } = useLang()
  const t = useCallback(
    (word: Word) => {
      return langs[lang][word]
    },
    [lang],
  )
  return { t, lang, setLang }
}
